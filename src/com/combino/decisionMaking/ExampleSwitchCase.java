package com.combino.decisionMaking;

public class ExampleSwitchCase {

    public String getRemarksBasedOnGrade(char grade) {

        switch (grade) {

            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                return "Very Good";
            case 'C':
                return "Good";
            case 'D':
                return "Fair";
            case 'E':
                return "Passed";
            case 'F':
                return "Failed";
            default:
                System.out.println("The grade is not valid");
        }
        return "";
    }

    public void exampleTerenaryOperator(int a) {
        boolean isEvenNumber;

        isEvenNumber = (a%2 ==0)? true: false;

        System.out.println(isEvenNumber);
    }

}
