package com.combino.decisionMaking;

public class ExampleDecisionMaking {

    private String[] animals = {"Dog", "Cat", "Lion", "Horse", "Cow", "Tiger"};
    private String[] birds = {"Hen", "Raven", "Crane", "Sparrow", "Kingfisher", "Humming"};

    public String isAnimalOrBird(String name) {

        for (String animal : animals
        ) {
            if (name == animal) {
                System.out.println(name + " is an animal");
                return "Animal";
            }
        }

        for (String bird : birds
        ) {
            if (name == bird) {
                System.out.println(name + " is a bird");
                return "Bird";
            }
        }

        System.out.println("Given name is not in our list");
        return "";

    }

    public void isGreaterThan() {
        int x = 7;
        if (x % 3 == 0) {
            System.out.println(x + " is divisible by 3");
        } else {
            System.out.println(x + " is not divisible by 3");

        }

    }

    public void isDivisible(int x) {

        if (x % 2 == 0) {
            System.out.println(x + " is divisible by 2");
            if( x%3==0){
                System.out.println(x + " is divisible by 3");
            }else {
                System.out.println(x + " is not divisible by 3");

            }
        }
        else{
            System.out.println(x + " is not divisible by 2");
            if( x%3==0){
                System.out.println(x + " is divisible by 3");
            }
        }
    }
}
