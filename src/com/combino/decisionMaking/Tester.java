package com.combino.decisionMaking;

public class Tester {
    public static void main(String[] args) {
        ExampleDecisionMaking decisionMaking = new ExampleDecisionMaking();
        System.out.println(decisionMaking.isAnimalOrBird("Humming"));
        decisionMaking.isGreaterThan();
        decisionMaking.isDivisible(4);
        decisionMaking.isDivisible(9);

        ExampleSwitchCase switchCase = new ExampleSwitchCase();
        System.out.println(switchCase.getRemarksBasedOnGrade('Z'));
        switchCase.exampleTerenaryOperator(100);
        switchCase.exampleTerenaryOperator(99);
    }
}
