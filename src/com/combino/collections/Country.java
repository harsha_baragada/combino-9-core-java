package com.combino.collections;

import java.util.List;

public class Country {

    private int id;
    private String name;
    private String capitalCity;
    private String president;
    private String primeMinisterName;
    private List<State> states;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapitalCity() {
        return capitalCity;
    }

    public void setCapitalCity(String capitalCity) {
        this.capitalCity = capitalCity;
    }

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president = president;
    }

    public String getPrimeMinisterName() {
        return primeMinisterName;
    }

    public void setPrimeMinisterName(String primeMinisterName) {
        this.primeMinisterName = primeMinisterName;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", capitalCity='" + capitalCity + '\'' +
                ", president='" + president + '\'' +
                ", primeMinisterName='" + primeMinisterName + '\'' +
                ", states=" + states +
                '}';
    }
}
