package com.combino.collections;

import java.util.*;

public class ExampleCollections {
    List<Country> countryCollection = new ArrayList<>();
    List<State> states = new Vector<>();

    public void printCollections() {

        /*
         * creation and adding India object to collection
         * */
        Country india = new Country();

        india.setId(1001);
        india.setCapitalCity("New Delhi");
        india.setName("India");
        india.setPresident("Kovind");
        india.setPrimeMinisterName("Modi");
        countryCollection.add(india);
        /*
         * creation and adding usa object to collection
         *  */
        Country usa = new Country();
        usa.setId(1002);
        usa.setCapitalCity("Washinton");
        usa.setName("USA");
        usa.setPresident("Joe biden");
        countryCollection.add(usa);
        System.out.println(countryCollection);


        for (Country country:countryCollection
             ) {
            if(country.getName().equalsIgnoreCase("USA"))
            System.out.println(country.getPresident());
        }
        System.out.println(countryCollection.get(1));

    }
}
