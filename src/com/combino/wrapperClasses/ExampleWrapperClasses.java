package com.combino.wrapperClasses;

import java.util.ArrayList;
import java.util.List;

public class ExampleWrapperClasses {
    String s ;
    Integer i = 1000;
    int anInt;

    public void exampleNumberClass() {
        Number number = 0;
        System.out.println(number);
        int a = number.intValue();
        System.out.println(a);
        double v = number.doubleValue();
        System.out.println(v);
        List<Integer> ints = new ArrayList<Integer>();
        Byte aByte;
        byte b;
        s = Integer.toString(i);
        Double aDouble;
        Float aFloat = 369.36F;
        aFloat.byteValue();
        Long aLong;
        Short aShort;
        Character character = '1';
        System.out.println(Character.isDigit(character));







    }
}
