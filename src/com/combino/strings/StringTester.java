package com.combino.strings;

public class StringTester {
    public static void main(String[] args) {

         int a =10;
        ExampleStrings exampleStrings = new ExampleStrings();
        exampleStrings.printStrings();
        exampleStrings.printConcatinatedStrings();
        exampleStrings.someMethod(a);
        exampleStrings.exampleStringBuilderAndStringBuffer();
    }
}
