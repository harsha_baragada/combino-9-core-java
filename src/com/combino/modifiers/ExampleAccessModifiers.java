package com.combino.modifiers;

public class ExampleAccessModifiers {
    //default
    int noOfPersons;
    //private
    private double weightOfThePerson;

    public String name;

    //protected
    protected int mobileNumber;

    public ExamplePrivateClass examplePrivateClass;



    public ExampleAccessModifiers() {
        System.out.println(weightOfThePerson);
    }

     class ExamplePrivateClass{

    }
}
