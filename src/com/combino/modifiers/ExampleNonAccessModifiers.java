package com.combino.modifiers;

public class ExampleNonAccessModifiers {

    public static int id = 102252;
    public final String city = "Delhi";
    public static final double PI= 3.14;
    public static final double FIXED_SALARY = 1200.50;

    public static void sampleStaticMethod() {
        System.out.println(id);
    }

    public void printVariables() {
        System.out.println(city);
    }
}
