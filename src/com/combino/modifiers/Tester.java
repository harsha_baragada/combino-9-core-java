package com.combino.modifiers;


public class Tester {

    public static void main(String[] args) {
        ExampleAccessModifiers exampleAccessModifiers = new ExampleAccessModifiers();
        System.out.println(exampleAccessModifiers.noOfPersons);
       // System.out.println(exampleAccessModifiers.weightOfThePerson);
        ExampleNonAccessModifiers.sampleStaticMethod();
        System.out.println(ExampleNonAccessModifiers.id);
        System.out.println(ExampleNonAccessModifiers.PI);
        double variableSalary = 856.20;
        double salary = variableSalary + ExampleNonAccessModifiers.FIXED_SALARY;
        System.out.println(salary);
        ExampleNonAccessModifiers exampleNonAccessModifiers = new ExampleNonAccessModifiers();
        exampleNonAccessModifiers.printVariables();
        Child child = new Child();
        child.aMethodInTheParent();
    }
}
