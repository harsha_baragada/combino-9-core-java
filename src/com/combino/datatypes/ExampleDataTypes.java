package com.combino.datatypes;

public class ExampleDataTypes {

    private byte aByte = -128;
    private short aShort= 32767;
    private  int anInt = 999999999;
    private  long aLong = 99999999999999999L;
    private float aFloat = 6996.3655F;
    private double cost = 96554.36;
    private char aChar = 'B';
    private boolean aBoolean = true;
    private Person person;

    public ExampleDataTypes() {
    }

    public ExampleDataTypes(byte aByte, double cost) {
        this.aByte = aByte;
        this.cost = cost;
    }

    public void printDataTypes(){

        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println(anInt);
        System.out.println(aLong);
        System.out.println(aFloat);
        System.out.println(cost);
        System.out.println(aChar);
        System.out.println(aBoolean);
        System.out.println(person);

    }



}
