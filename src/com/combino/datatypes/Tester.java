package com.combino.datatypes;

import com.combino.variables.VariableTypes;

public class Tester {
    public static void main(String[] args) {
        byte b = 108;

        ExampleDataTypes dataTypes = new ExampleDataTypes();
        dataTypes.printDataTypes();

        ExampleDataTypes exampleDataTypes = new ExampleDataTypes(b, 369.963D);
        exampleDataTypes.printDataTypes();

        SalaryCalculator peterSalary = new SalaryCalculator(1200.00d, 620.36D, "Peter");

        System.out.println(peterSalary.calculateTotalSalary());

        SalaryCalculator bobSalary = new SalaryCalculator(1200.00D, 820.36D, "Bob");

        System.out.println(bobSalary.calculateTotalSalary());
        System.out.println("From Data types tester " + VariableTypes.COUNTRY_NAME);
    }
}
