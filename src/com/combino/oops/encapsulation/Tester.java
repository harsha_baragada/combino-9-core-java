package com.combino.oops.encapsulation;

public class Tester {
    public static void main(String[] args) {
        Person person = new Person("Emile", 181.36, 81.59);
        person.setName("Max");
        System.out.println(person.getName());

    }
}
