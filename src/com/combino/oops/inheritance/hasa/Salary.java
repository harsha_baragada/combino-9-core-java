package com.combino.oops.inheritance.hasa;

public class Salary {
    private double fixedSalary = 50D;
    private double variableSalary;
    private double allowances;
    private double bonus;

    public double getFixedSalary() {
        return fixedSalary;
    }

    public void setFixedSalary(double fixedSalary) {
        this.fixedSalary = fixedSalary;
    }

    public double getVariableSalary() {
        return variableSalary;
    }

    public void setVariableSalary(double variableSalary) {
        this.variableSalary = variableSalary;
    }

    public double getAllowances() {
        return allowances;
    }

    public void setAllowances(double allowances) {
        this.allowances = allowances;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}
