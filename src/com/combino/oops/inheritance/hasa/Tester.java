package com.combino.oops.inheritance.hasa;

public class Tester {
    public static void main(String[] args) {
        DailyWageWorker dailyWageWorker = new DailyWageWorker();
        System.out.println(dailyWageWorker.calculateWage());
    }
}
