package com.combino.oops.inheritance.hasa;

public class DailyWageWorker extends Person{

    private float noOfHours = 8;

    public double calculateWage(){
        return noOfHours * super.getSalary().getFixedSalary();
    }

}
