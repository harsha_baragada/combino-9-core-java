package com.combino.oops.inheritance;

public class ExampleStaticMethod {
    public static void sampleMethod() {
        System.out.println("This is just an example static method");
    }
}
