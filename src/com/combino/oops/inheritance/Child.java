package com.combino.oops.inheritance;

public class Child extends Parent{

    public Child(String name, int id, boolean isGood) {
        super(name, id, isGood);
        System.out.println("You just invoked the constructor of the child class with parameters");
    }


    public Child() {
        System.out.println("You just invoked the constructor of the child class");
    }

    public void methodFromParent() {
        System.out.println("This is a method from child class");
    }
}
