package com.combino.oops.overloadingAndOverriding;

public class Circle extends Square {

    private int pi = 3;
    private int radius = 5;

    @Override
    public int calculateTheArea() {
        System.out.println(super.calculateTheArea());
        return pi * radius * radius;
    }


}
