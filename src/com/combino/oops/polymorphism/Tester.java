package com.combino.oops.polymorphism;

public class Tester {
    public static void main(String[] args) {

        RegularEmployee regularEmployee = new RegularEmployee();
        Person person = new RegularEmployee();
        Employee employee = new RegularEmployee();
        Object o = new RegularEmployee();

        System.out.println(regularEmployee instanceof RegularEmployee);
        System.out.println(person instanceof Person);
        System.out.println(employee instanceof Employee);
        System.out.println(o instanceof Object);

    }
}
