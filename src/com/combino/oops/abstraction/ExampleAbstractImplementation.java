package com.combino.oops.abstraction;

public class ExampleAbstractImplementation extends ExampleAbstractClass{

    @Override
    public void exampleAbstractMethod() {
        System.out.println("Implementing abstract method from the abstract parent");
    }
}
