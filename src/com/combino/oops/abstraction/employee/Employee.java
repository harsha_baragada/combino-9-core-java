package com.combino.oops.abstraction.employee;

public abstract class Employee {

    public Employee() {
    }

    public abstract double calculateSalary();
}
