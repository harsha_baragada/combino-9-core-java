package com.combino.oops.abstraction;

import com.combino.oops.abstraction.employee.DailyWageEmployee;
import com.combino.oops.abstraction.employee.Employee;
import com.combino.oops.abstraction.employee.RegularEmployee;

public class Tester {
    public static void main(String[] args) {

        ExampleAbstractClass exampleAbstractClass = new ExampleAbstractImplementation();
        exampleAbstractClass.exampleAbstractMethod();
        OneMoreImplementation oneMoreImplementation = new OneMoreAbstractImplementation();

        Employee dailyWageEmployee = new DailyWageEmployee();
        System.out.println(dailyWageEmployee.calculateSalary() + " per day");
        Employee regulaEmployee = new RegularEmployee();
        System.out.println(regulaEmployee.calculateSalary() + " per month");
    }
}
