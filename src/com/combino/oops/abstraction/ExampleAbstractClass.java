package com.combino.oops.abstraction;

public abstract class ExampleAbstractClass {

    public void someMethod() {
    }

    public abstract void exampleAbstractMethod();
}
