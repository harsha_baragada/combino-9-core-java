package com.combino.oops.interfaces.animal;

public interface Mammal extends Animal{


    String givesMilk();
}
