package com.combino.oops.interfaces.animal;

public interface Animal {

    void eat();

    void walk();

    void run();
}
