package com.combino.oops.interfaces.animal;

public class Tester {
    public static void main(String[] args) {
        Animal animal = new Deer();
        Mammal mammal = new Deer();
        Deer deer = new Deer();
        System.out.println(mammal instanceof Animal);
        System.out.println(deer instanceof Animal);
    }
}
