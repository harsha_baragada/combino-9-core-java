package com.combino.oops.interfaces.animal;

public class Deer implements Mammal{

    @Override
    public void eat() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void run() {

    }

    @Override
    public String givesMilk() {
        return null;
    }

    public boolean hasHorns(){
        return true;
    }

}
