package com.combino.dataStructures;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Vector;

public class ExampleEnumeration {

    Enumeration fruits;
    BitSet bitSet;

    Vector fruitNames = new Vector();

    public void printFruitNames(){

        fruitNames.add("Apple");
        fruitNames.add("Banana");
        fruitNames.add("Strawberry");
        fruitNames.add("Mango");
        fruitNames.add("Peach");

        fruits = fruitNames.elements();
        while (fruits.hasMoreElements()){
            System.out.println(fruits.nextElement());
        }

        for(int i = 0; i< fruitNames.size() ; i++){
            System.out.println(fruitNames.get(i));
        }

        for (Object fruit:fruitNames
             ) {
            System.out.println(fruit);
        }
    }

}
