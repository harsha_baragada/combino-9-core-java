package com.combino.dataStructures;

import java.util.Dictionary;
import java.util.Hashtable;

public class ExampleDictionary {

    Dictionary<Integer,String> dictionary = new Hashtable<>();
    Hashtable<Integer,String> hashtable = new Hashtable<>();
    Dictionary<Dictionary<Integer,String>,String[]> dictionaryIntegerDictionary = new Hashtable<>();
    public void printHashTableMethods(){


        dictionary.put(1001,"Zara");
        dictionary.put(1002,"Eva");
        dictionary.put(9698,"Peter");
        dictionary.put(9965,"Sander");
        dictionary.put(5008,"Virat");
        dictionary.put(7200,"David");
        dictionary.put(9999,"James");
        System.out.println(dictionary);
        System.out.println(dictionary.size());
        System.out.println(dictionary.isEmpty());
        System.out.println(dictionary.remove(1001));
        System.out.println(dictionary);

        String[] strings = {"sdsds","sdsd"};
        dictionaryIntegerDictionary.put(dictionary,strings);
        System.out.println(dictionaryIntegerDictionary);



        hashtable.put(1001,"Zara");
        hashtable.put(1002,"Eva");
        hashtable.put(9698,"Peter");
        System.out.println(hashtable.contains("1001"));
        System.out.println(System.getProperties());


    }
}
