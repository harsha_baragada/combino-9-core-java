package com.combino.dataStructures;

import java.util.BitSet;
import java.util.Stack;

public class ExampleBitSet {

    public void printBitSet(){
        BitSet bitSet = new BitSet(16);
        BitSet bitSet1 = new BitSet(16);

        for (int i=0; i<16; i++){
            if(i%2 ==0) bitSet.set(i);
            if(i%3 !=0) bitSet1.set(i);
        }
        System.out.println(bitSet);
        System.out.println(bitSet1);

        bitSet.and(bitSet1);
        System.out.println(bitSet);

        bitSet.or(bitSet1);
        System.out.println(bitSet);

        Stack<String> stack = new Stack<>();
        stack.push("Rich dad poor dad");
        stack.push("Psychology of money");
        stack.push("$100 startup");
        stack.push("Zero to one");
        System.out.println(stack);
        System.out.println(stack.peek());
        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack);
        System.out.println(stack.empty());
        System.out.println(stack.search("Psychology of money"));



    }
}
