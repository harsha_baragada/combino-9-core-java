package com.combino.dataStructures;

public class Tester {
    public static void main(String[] args) {
        ExampleEnumeration exampleEnumeration = new ExampleEnumeration();
        exampleEnumeration.printFruitNames();

        ExampleBitSet bitSet= new ExampleBitSet();
        bitSet.printBitSet();

        ExampleDictionary dictionary = new ExampleDictionary();
        dictionary.printHashTableMethods();
    }
}
