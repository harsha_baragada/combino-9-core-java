package com.combino.dateAndTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ExampleGregorianCalender {

    public void printCalender(){
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        System.out.println(gregorianCalendar);
        GregorianCalendar calendar = new GregorianCalendar(2000,01,28,14,50,36);
        System.out.println(calendar);
        System.out.println(gregorianCalendar.isLeapYear(2001));
        System.out.println(gregorianCalendar.equals(calendar));
        System.out.println(gregorianCalendar.getTime().getHours());
        System.out.println(gregorianCalendar.get(Calendar.DAY_OF_MONTH));
        gregorianCalendar.roll(Calendar.MONTH,false);
        System.out.println(gregorianCalendar);
    }


    /* character            description             -           example
    * c             -      complete date and time   -       Sat Dec 18 05:55:24 IST 2021
    * T             -     24 hours                  -       13:00:52
    * Y             -       Year                    -       4 digits of the year
    * y             -       year                    -       2 digits of the year
    * d             -       day                     -       represents the day
    * M             -       Month                   -       represents the month
    * m             -       Minutes                 -
    * S             -       Seconds
    * Z             -       Time zone
    * */
    public void dateFormatting(){

        // 18 Dec, 2021
        SimpleDateFormat dateFormat = new SimpleDateFormat(" HH:mm:ss dd MMM, yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

    }
}
