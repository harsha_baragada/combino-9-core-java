package com.combino.dateAndTime;

import java.util.Date;

public class ExampleDate {

    Date now = new Date();

    public void printDates() throws InterruptedException {
        System.out.println(now +" printing now");
        Thread.sleep(2000);
        Date later = new Date();
        System.out.println(later+" printing later");
        // Deprecated constructor
        Date date = new Date(2012, 11, 21);
       //
        System.out.println(later.after(now));
        System.out.println(now.before(later));
        System.out.println(now.equals(later));
        System.out.println(now.compareTo(now));
        System.out.println(now.hashCode());
        System.out.println(now.toString());
        System.out.println(now.getTime());
        later.setTime(now.getTime());
        System.out.println(later);
        System.out.println(now.getDate());
        System.out.println(now.getYear());
        System.out.println(now.getTimezoneOffset());
        System.out.println(now.getMinutes());
        System.out.println(now.getDay());

    }
}
