package com.combino.dateAndTime;

public class DateTester {
    public static void main(String[] args) throws InterruptedException {
        ExampleDate date = new ExampleDate();
        date.printDates();
        ExampleGregorianCalender gregorianCalender = new ExampleGregorianCalender();
        gregorianCalender.printCalender();
        gregorianCalender.dateFormatting();

    }
}
