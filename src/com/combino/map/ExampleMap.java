package com.combino.map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExampleMap {
    Map<Integer, String> map = new LinkedHashMap<>();

    public void methodsOfMap() {
        map.put(1005, "Emile");
        map.put(1005, "Emile");
        map.put(1004, "Sandy");
        map.put(1001, "Zara");
        map.put(1003, "Mike");
        map.put(1006, "Peter");
        map.put(1002, "Max");

        System.out.println(map);
        System.out.println(map.size());
        System.out.println(map.keySet());
        System.out.println(map.values());

    }
}
