package com.combino.loops;

public class ExampleLoops {

    private int ids[] = {1001,1002,1003,1005,1102,1554,1556,9999};

    private int x = 40;


    /*
    * for(intialization; boolean_expression; update){
    *
    *
    * }
    *
    * */
    public void genericForLoop(){

        for(int i =10; i>=0 ; i--){
            System.out.println(i);
        }
    }

    public void exampleForEachLoop(){
        System.out.println("******Example for each loop**********");
        for (int id: ids) {
            System.out.println(id);
        }

    }

    /*
    * while(boolean_expression){
    *
    * }
    * */

    public void exampleWhileLoop(){
        System.out.println("************Example while loop*********");

        while (x >0){
            System.out.println(x);
            x--;
        }
    }


    public void exampleDoWhile(){
        System.out.println("************Example do while loop*********");


        do{
            System.out.println(x);
            x++;
        }
        while (x >=40);
    }
}
