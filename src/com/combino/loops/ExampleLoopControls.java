package com.combino.loops;

public class ExampleLoopControls {


    public void printLoopWithControls(){
        System.out.println(">>>>>>>>>>>>>>>Loop controls<<<<<<<<<<<<<<<");
        for (int x =0; x < 20 ; x++){
            if(x == 10){
                break;
            }
            System.out.println(x);
        }
        System.out.println(">>>>>>>>>>>>>>>Out side for break<<<<<<<<<<<<<<<");

        for (int x =0; x < 20 ; x++){
            if(x == 10){
                continue;
            }
            System.out.println(x);
        }
        System.out.println(">>>>>>>>>>>>>>>Out side for continue<<<<<<<<<<<<<<<");


    }
}
