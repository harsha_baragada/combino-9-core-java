package com.combino.loops;

public class Tester {

    public static void main(String[] args) {
        ExampleLoops loops = new ExampleLoops();
        loops.genericForLoop();
        loops.exampleForEachLoop();
        loops.exampleWhileLoop();
        loops.exampleDoWhile();
        ExampleLoopControls exampleLoopControls = new ExampleLoopControls();
        exampleLoopControls.printLoopWithControls();
    }
}
