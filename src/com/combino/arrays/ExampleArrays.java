package com.combino.arrays;

import java.util.Arrays;
import java.util.Collections;

public class ExampleArrays {

    // preferred way
    int[] ids  = {1701,1002,1053,9004,1045};

    // this will also work but not to be preferred
    int rollNumbers[]= {9999,9998,9997,9994,9995};

   int[] vehicleNumbers = new int[10];
   Person[] people;


    public void printArrays(int a){

        Arrays arrays;


        System.out.println(ids.length);
        int[] clonedArray = ids.clone();
        System.out.println(Arrays.equals(ids,clonedArray));
        Arrays.fill(clonedArray,1053);
        Arrays.sort(ids);
       // Arrays.sort(clonedArray, Collections.reverseOrder());
        System.out.println(Arrays.binarySearch(ids,a));
        for (int i: ids
             ) {
            System.out.println(i);
        }
        for (int j: clonedArray){
            System.out.println(j);
        }
    }


}
