package com.combino.variables;

import com.combino.modifiers.ExampleAccessModifiers;

public class SampleAccessModifiers extends ExampleAccessModifiers{

    public void justASampleMethod(){
        SampleAccessModifiers exampleAccessModifiers = new SampleAccessModifiers();
        System.out.println(exampleAccessModifiers.mobileNumber);
    }
}
