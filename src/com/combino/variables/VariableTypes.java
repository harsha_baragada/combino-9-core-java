package com.combino.variables;

import com.combino.modifiers.ExampleAccessModifiers;

public class VariableTypes extends ExampleAccessModifiers {
    private String name;
    boolean isDone;
    public static final String COUNTRY_NAME = "USA";

    public VariableTypes(String name, boolean isDone) {
        this.name = name;
        this.isDone = isDone;
    }

    public VariableTypes() {
    }

    public void printLocalVariables() {
        System.out.println("first method starts");
        int a = 10;
        System.out.println(a);
        System.out.println(name);
    }


    public void secondMethod() {
        System.out.println(name);
    }
}
