package com.combino.exceptions;

import java.io.EOFException;
import java.io.FileNotFoundException;

public class ExceptionTester {
    public static void main(String[] args) throws FileNotFoundException, EOFException, InterruptedException {
        ExampleExceptions exampleExceptions = new ExampleExceptions();
        exampleExceptions.readFile();
        exampleExceptions.exampleCheckedExceptions();
    }
}
