package com.combino.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExampleExceptions {

    String city = "Dallas";

    public void readFile() {
        boolean isError = false;
        try {
            File file = new File("C:/Users/VIHCADE/Desktop/sample.txt");
            FileReader fileReader = new FileReader(file);
        }  catch (MyOwnException exception) {
            System.out.println("own exception");
        }
        catch (FileNotFoundException fileNotFoundException) {
            isError = true;
            System.out.println("The file you're trying to process is not in place, please place the file on your desktop and try again.!");
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("You're trying to access the elements beyond the range of an array..!!");
        }catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (isError == true) {
                System.out.println("The file process has been completed with an error");

            } else {
                System.out.println("The file process has been completed without an error");
            }
        }


    }

    public void exampleCheckedExceptions() {
        try {
            String[] fruits = {"Banana", "Apple", "Mango", "Orange", "Grapes", "Strawberries"};
            System.out.println(fruits[6]);
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("You're trying to access the elements beyond the range of an array..!!");
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    public void someMethod() throws Exception{

        int id;

    }
}
