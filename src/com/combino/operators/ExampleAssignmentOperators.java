package com.combino.operators;

public class ExampleAssignmentOperators {
    private int a = 40;

    private double fixedSalary = 1280.25D;
    private double variableSalary;



    public void printVariables() {

        System.out.println(a);
        a += 10;
        System.out.println(a);
        a -= 5;
        System.out.println(a);
        a *= 5;
        System.out.println(a);

        a /= 10;
        System.out.println(a);

        a %= 5;
        System.out.println(a);

        a >>= 2;
        System.out.println(a);


        a <<= 2;
        System.out.println(a);


        a &= 2;
        System.out.println(a);

        a |= 2;
        System.out.println(a);

        a ^= 2;
        System.out.println(a);
    }

    public double calculateSalary(boolean isSaturdayWorkDay){
        // terenary operator

        //assignment variable = (condition) ? firstValue   : secondValue;
        variableSalary = isSaturdayWorkDay? 600 : 0;

        if(isSaturdayWorkDay){
            variableSalary = 600;
        }
        else{
            variableSalary = 0;
        }


        return fixedSalary + variableSalary;

    }
}
