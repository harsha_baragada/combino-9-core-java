package com.combino.operators;

public class ExampleRelationalOperators {

    public boolean equals(double a, double b) {
        return a == b;
    }

    public boolean notEquals(double a, double b) {
        return a != b;
    }

    public boolean greaterThan(double a, double b) {
        return a > b;
    }

    public boolean lessThan(double a, double b) {
        return a < b;
    }

    public boolean greaterThanOrEquals(double a, double b) {
        return a >= b;
    }

    public boolean lessThanOrEquals(double a, double b) {
        return a <= b;
    }
}
