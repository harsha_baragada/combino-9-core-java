package com.combino.operators;

public class ExampleArithmeticVariables {

    public int sum(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int division(int a, int b) {
        return a / b;
    }

    public int modulus(int a, int b) {
        return a % b;
    }

    public int increment(int a) {
        return a++ ;
    }

    public int decrement(int a) {
        return a-- ;
    }
}
