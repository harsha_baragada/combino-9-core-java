package com.combino.operators;

public class Tester {

    public static void main(String[] args) {
        int x = 20, something = 5680;
        double a = 698.36, b = 9863.25;
        boolean e = true, f = false;

        ExampleArithmeticVariables exampleArithmeticVariables = new ExampleArithmeticVariables();
        System.out.println("addition of " + x + " and " + something + " is " + exampleArithmeticVariables.sum(x, something));
        System.out.println("subtraction of " + x + " and " + something + " is " + exampleArithmeticVariables.subtraction(x, something));
        System.out.println("multiply of " + x + " and " + something + " is " + exampleArithmeticVariables.multiply(x, something));
        System.out.println("division of " + x + " and " + something + " is " + exampleArithmeticVariables.division(x, something));
        System.out.println("modulus of " + x + " and " + something + " is " + exampleArithmeticVariables.modulus(x, something));
        System.out.println("increment of " + x + " and " + something + " is " + exampleArithmeticVariables.increment(x));
        System.out.println("decrement of " + x + " and " + something + " is " + exampleArithmeticVariables.decrement(x));

        System.out.println("******************Relational operators starts*********************");
        ExampleRelationalOperators relationalOperators = new ExampleRelationalOperators();
        System.out.println(relationalOperators.equals(a,b));
        System.out.println(relationalOperators.notEquals(a,b));
        System.out.println(relationalOperators.greaterThan(a,b));
        System.out.println(relationalOperators.lessThan(a,b));
        System.out.println(relationalOperators.greaterThanOrEquals(a,b));
        System.out.println(relationalOperators.lessThanOrEquals(a,b));

        System.out.println("*****************Bitwise operators************");
        ExampleBitwiseOperators bitwiseOperators = new ExampleBitwiseOperators();
        System.out.println(bitwiseOperators.and(x,something));
        System.out.println(bitwiseOperators.or(x,something));
        System.out.println(bitwiseOperators.xor(x,something));
        System.out.println(bitwiseOperators.compliment(something));
        System.out.println(bitwiseOperators.rightShift(something));
        System.out.println(bitwiseOperators.leftShift(something));

        System.out.println("****************Logical operators*************");
        ExampleLogicalOperators logicalOperators  = new ExampleLogicalOperators();
        System.out.println(logicalOperators.and(e,f));
        System.out.println(logicalOperators.or(e,f));
        System.out.println(logicalOperators.not(f));

        System.out.println("*********************Assignment Operators*************");
        ExampleAssignmentOperators assignmentOperators = new ExampleAssignmentOperators();
        assignmentOperators.printVariables();

        System.out.println(assignmentOperators.calculateSalary(true));
        System.out.println(assignmentOperators.calculateSalary(false));


    }

}
