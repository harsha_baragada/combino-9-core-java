package com.combino.operators;

public class ExampleLogicalOperators {


    private boolean isAccountActive;
    private double balance;
    private double requestedWithdrawlAmount;

    public boolean and(boolean a, boolean b) {
        return a && b;
    }

    public boolean or(boolean a, boolean b) {
        return a || b;
    }

    public boolean not(boolean a) {
        return !a;
    }

    /*
     * isAccountActive
     * balance
     *
     *
     *
     *
     * */


    public boolean permissionForBalanceWithdrawl() {
        return isAccountActive && (balance > requestedWithdrawlAmount);
    }


}