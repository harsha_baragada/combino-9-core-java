package com.combino.operators;

public class ExampleBitwiseOperators {

    public int and(int a, int b) {
        return a & b;
    }

    public int or(int a, int b) {
        return a | b;
    }

    public int xor(int a, int b) {
        return a ^ b;
    }

    public int compliment(int b) {
        return ~b;
    }


    public int rightShift(int a) {
        return a >> 2;
    }

    public int leftShift(int a) {
        return a<<2;
    }

}
