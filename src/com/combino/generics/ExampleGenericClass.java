package com.combino.generics;

public class ExampleGenericClass <E>{
    private E e;
    private String name;
    private int id;
    private long mobileNumber;

    public ExampleGenericClass(E e) {
        this.e = e;
    }

    public ExampleGenericClass() {
    }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
