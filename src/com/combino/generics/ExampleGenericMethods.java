package com.combino.generics;

import com.combino.collections.set.Person;

public class ExampleGenericMethods {

    public  <E> void printArrayOfElements(E[] arrayInput){

        for (E e:arrayInput
             ) {
            if(e instanceof Person){
                System.out.println(((Person) e).getName());
            }
            System.out.println(e);
        }
    }
}
