package com.combino.generics;

import com.combino.collections.set.Person;

public class Tester {

    public static void main(String[] args) {
        Integer[] ids = {1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009};
        String[] fruits = {"Apple", "Banana", "Papaya", "Blueberry"};
        Person peter = new Person(5002,25,"peter",0.00,null);
        Person mark = new Person(6955,28,"Mark",0.00,null);
        Person feye = new Person(9999,25,"Feye",0.00,null);
        Person[] people ={peter,mark,feye} ;
        System.out.println(people);

        ExampleGenericMethods genericMethods = new ExampleGenericMethods();
        genericMethods.printArrayOfElements(ids);
        genericMethods.printArrayOfElements(fruits);
        genericMethods.printArrayOfElements(people);

        ExampleGenericClass<Integer> integerExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<String> stringExampleGenericClass = new ExampleGenericClass<>();

        integerExampleGenericClass.setE(1001);
        stringExampleGenericClass.setE("Some String value");

        System.out.println(integerExampleGenericClass.getE());
        System.out.println(stringExampleGenericClass.getE());
    }
}
