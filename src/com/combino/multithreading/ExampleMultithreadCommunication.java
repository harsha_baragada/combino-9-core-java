package com.combino.multithreading;

public class ExampleMultithreadCommunication {

    boolean flag = false;

    public synchronized  void  question(String message){
        if(flag){
            try{
                wait();
            }
            catch (InterruptedException e){
                System.out.println("There is no response");
            }
            System.out.println(message);
            flag= true;
            notify();
        }
    }

    public synchronized  void  answer(String message){
        if(!flag){
            try{
                wait();
            }
            catch (InterruptedException e){
                System.out.println("There is no question");
            }
            System.out.println(message);
            flag= true;
            notify();
        }
    }
}


class Thread1 implements Runnable{
    ExampleMultithreadCommunication communication;
    String[] s1 ={"Hi", "How are you", "I am also good"};

    public Thread1(ExampleMultithreadCommunication communication) {
        this.communication = communication;
        new Thread(this,"question").start();
    }

    @Override
    public void run() {

        for (int i = 0 ;i < s1.length;i ++){
            communication.question(s1[i]);
        }
    }


}

class Thread2 implements Runnable{
    ExampleMultithreadCommunication communication;
    String[] responses ={"Hi", "I am good, what about you", "cool!!"};

    public Thread2(ExampleMultithreadCommunication communication) {
        this.communication = communication;
        new Thread(this,"answer").start();
    }

    @Override
    public void run() {

        for (int i = 0 ;i < responses.length;i ++){
            communication.question(responses[i]);
        }
    }
}