package com.combino.multithreading;

public class ExampleDeadlock {
    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    static class DeadLockThread1 extends Thread {


        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 1 is holding for the lock 1");
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("The running of the thread has been interrupted");
                }
                System.out.println("Thread 1 is waiting for the lock 2");
                synchronized (lock2) {
                    System.out.println("Thread 1 is holding for the lock 2");
                }
            }
        }
    }

    static class DeadLockThread2 extends Thread{
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 2 is holding for the lock 2");
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("The running of the thread has been interrupted");
                }
                System.out.println("Thread 2 is waiting for the lock 1");
                synchronized (lock2) {
                    System.out.println("Thread 2 is holding for the lock 1");
                }
            }
        }
    }
}


