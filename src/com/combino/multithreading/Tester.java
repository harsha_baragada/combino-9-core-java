package com.combino.multithreading;

public class Tester {
    public static void main(String[] args) {

       /* ExampleMultiThreading threading1 = new ExampleMultiThreading("thread 1");
        threading1.start();*/
        ExampleMultiThreading threading2 = new ExampleMultiThreading("Thread 2");
        threading2.start();

        System.out.println("**********************************************************");
        ExampleSynchronization synchronization = new ExampleSynchronization();
        DemoThread thread1 = new DemoThread("Thread-1", synchronization);
        DemoThread thread2 = new DemoThread("Thread-2", synchronization);

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();

        } catch (Exception e) {
            System.out.println("programme interrupted");
        }
    }

}
