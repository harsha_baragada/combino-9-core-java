package com.combino.multithreading;

public class CommunicationTester {
    public static void main(String[] args) {
        ExampleMultithreadCommunication communication = new ExampleMultithreadCommunication();
        new Thread1(communication);
        new Thread2(communication);
    }
}
