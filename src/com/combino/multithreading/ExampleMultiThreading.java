package com.combino.multithreading;

public class ExampleMultiThreading implements Runnable {

    private Thread thread;
    private String threadName;

    public ExampleMultiThreading(String threadName) {
        this.threadName = threadName;
        System.out.println(threadName + " has been created!!");
    }

    @Override
    public void run() {
        System.out.println(threadName + " has been started running!!");

        try {
            for (int i = 4; i > 0; i--) {
                System.out.println(threadName + " has been running " + i);
                Thread.sleep(4000);
            }

        } catch (InterruptedException e) {
            System.out.println("The programme has been interrupted!!");
        }
        System.out.println("We are done with " + threadName);
    }

    public void start() {
        System.out.println(threadName + " has been started running");
        if (thread == null) {
            thread = new Thread(this, threadName);
            thread.start();
        }
    }
}
