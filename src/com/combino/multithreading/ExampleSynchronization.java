package com.combino.multithreading;

public class ExampleSynchronization {

    public void printCount() {
        try {

            for (int i = 0; i < 4; i++) {
                System.out.println("counter - " + i);
            }
        } catch (Exception e) {
            System.out.println("Program interrupted");
        }

    }

    public void someOtherMethod(){

    }
}

class DemoThread extends Thread {
    private Thread thread;
    private String threadName;
     ExampleSynchronization sync;

    public DemoThread(String name, ExampleSynchronization sync) {
        super(name);
        this.threadName = name;
        this.sync = sync;
    }

    public void run() {
        synchronized (sync) {
            sync.printCount();
        }
        System.out.println("Thread " + threadName + " existing.");
    }

    public void start() {
        System.out.println(threadName + " is starting");
        if(thread == null){
            thread= new Thread(this,threadName);
            thread.start();
        }
    }
}

