package com.combino.multithreading;

public class DeadBlockTester {
    public static void main(String[] args) {
        ExampleDeadlock.DeadLockThread1 thread1 = new ExampleDeadlock.DeadLockThread1();
        thread1.start();
        ExampleDeadlock.DeadLockThread2 thread2 = new ExampleDeadlock.DeadLockThread2();
        thread2.start();
    }
}
